package com.goindigo.in.DFDENGINE;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.log4j.Logger;

public class GetTailNumber {

	private static final transient Logger LOGGER = Logger.getLogger(GetTailNumber.class);

	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	   public void GetTailNumber(Exchange exchange) {

		
			String str = exchange.getIn().getHeader("CamelFileName", String.class);
			String body =(String) exchange.getIn().getBody(String.class).toString();
			if(body.contains("AC_TAIL"))
			{
			 int  intialindex=body.indexOf("AC_TAIL=.".trim())+9;
		    LOGGER.info("DED_Engine filename in java"+str);
		    LOGGER.info("DFD_ENGINE in java=============="+ body.substring(intialindex,intialindex+6));
		     String TailNumber=body.substring(intialindex,intialindex+6);
		     exchange.getIn().setHeader("tail",TailNumber);
			}
		
		}	

}
