package com.goindigo.in.DFDENGINE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileFilter;
import org.apache.log4j.Logger;

public class DfdData implements GenericFileFilter {

	private static final transient Logger LOGGER = Logger.getLogger(DfdData.class);

	/**
	 * This method insert values in the DB.
	 * @param exchange
	 */
	public synchronized void  insert(Exchange exchange) {

		try {
			String str = exchange.getIn().getHeader("CamelFileName", String.class);
			LOGGER.info("GSAServerAutomationATR FileName="+str);
			//ZipFileName = REPORT02.zip, Date = 17-12-13, Time = 12:09:07:652,          Size = 2443, File Path , File length = 2443
			    String fileName = str.substring(str.lastIndexOf("/")+1,str.length());
			    exchange.getIn().setHeader("finalfilename", fileName);
			String fileSize = exchange.getIn().getHeader("CamelFileLength", String.class);
			String filePath = exchange.getIn().getHeader("CamelFileNameProduced", String.class);
			String fileDate = exchange.getIn().getHeader("fileDate", String.class);
			String filePickedTime = exchange.getIn().getHeader("filePickedTime", String.class);
			String filePushedTime = exchange.getIn().getHeader("filePushedTime", String.class);
			String fileData = exchange.getIn().getHeader("fileBody", String.class);
			String fileSourcePath = exchange.getIn().getHeader("CamelFilePath", String.class);
			String FileType = exchange.getIn().getHeader("FileType", String.class);

			String camelFileNameConsumed = exchange.getIn().getHeader("CamelFileNameConsumed", String.class);

			String tailName = "";
			//if(camelFileNameConsumed.toLowerCase().contains("vt"))
			//	tailName = camelFileNameConsumed.substring(camelFileNameConsumed.toLowerCase().indexOf("vt"),camelFileNameConsumed.toLowerCase().indexOf("vt")+6);
			
			tailName = getFileName(exchange);
			
			  String zipFileName = camelFileNameConsumed.toString();

			  String extension = str.substring(str.lastIndexOf(".")+1,str.length());
			//String folderName = str.substring(str.indexOf("/")+1,str.lastIndexOf("/"));
			  String folderName = "";

			//System.out.println("FileName ="+fileName + " FileSize ="+fileSize+" FilePath ="+filePath+" FileDate ="+fileDate+" FilePickedTime ="+filePickedTime+" FilePushedTime ="+filePushedTime);
			String flightDate = "";
			//String tailName = "";
			if(fileData.contains("DATE=")) {
				System.out.println("flightdata@@@@@@@@@@@@@================"+fileData.substring(fileData.indexOf("DATE=")+5,11));
				String strDate = fileData.substring(fileData.indexOf("DATE=")+5,11);
				//tailName = fileData.substring(fileData.indexOf("TAIL=")+6,fileData.indexOf("TAIL=")+12);

				if(!strDate.equals("")) {
					SimpleDateFormat parser = new SimpleDateFormat("ddMMyy");
					Date date;
					try {
						date = parser.parse(strDate);

						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						flightDate = formatter.format(date);

						System.out.println( flightDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						LOGGER.info("GSAServerAutomationATR ParseException FileName="+e.getMessage());
					}
				}
			}


			exchange.getIn().setHeader("fileDate", fileDate);
			exchange.getIn().setHeader("fileData", fileData);
			exchange.getIn().setHeader("fileSourcePath", fileSourcePath);
			exchange.getIn().setHeader("zipFileName", zipFileName);
			exchange.getIn().setHeader("flightDate", flightDate);
			exchange.getIn().setHeader("fileSize", fileSize);                       

		} catch(Exception e) {
			//e.printStackTrace();
			LOGGER.info("Exception GSAServerAutomationATR FileName="+e.getMessage());
		}
	}

	public String getFileName(Exchange exchange)
	{
		String filename = "";
		String camelFileName = exchange.getIn().getHeader("CamelFileName").toString();
		LOGGER.info("filename is equal to"+filename);
		LOGGER.info("camelfilename=============="+camelFileName);

		if(camelFileName.contains("VT"))
		{
			//String body =(String) exchange.getIn().getBody(String.class).toString();
			String body =(String) exchange.getIn().getHeader("fileBody");

			
			if(body.contains("AC_TAIL"))
			{
			 int  intialindex=body.indexOf("AC_TAIL=.".trim())+9;
		     System.out.println("body in java@@@@@@@@@@@@@@@"+ body.substring(intialindex,intialindex+6));
		     String TailNumber=body.substring(intialindex,intialindex+6);
		     exchange.getIn().setHeader("tail",TailNumber);
			}
			filename = camelFileName.substring(camelFileName.indexOf("VT"),camelFileName.indexOf("VT")+6);
			//exchange.getIn().setHeader("filename",filename);
		}
		else {
			LOGGER.info("invalid filename ");
		}
		
		return filename;
	}

	@Override
	public boolean accept(GenericFile file) {
		String filename=file.getFileName().toString().toLowerCase();
    	boolean status=false;
        if (filename.contains("vt".toLowerCase()) && filename.contains("r.zip")) {
        	
         status=true;
            
        }
        return status;
    }
	}


